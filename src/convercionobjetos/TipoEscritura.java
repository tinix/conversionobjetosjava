package convercionobjetos;
/**
 * @author tinix
 */
public class TipoEscritura {
    
    public void CLASICO(){
        System.out.println("Escritura a Mano" );
    } 
    
    public void MODERNO(){
        System.out.println("Escritura digital");
        
    }

    private final String description;

    private TipoEscritura(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }
}
