package convercionobjetos;

        
/**
 *
 * @author tinix
 */
public class ConvercionObjetos {

    public static void main(String[] args) {
        Empleado empleado;

        empleado = new Empleado("Tinix", 40000, TipoEscritura.CLASICO); // conversion upcasting

        imprimirDetalles(empleado);

        empleado  = new Gerente("Laura" , 18000, "Sistemas");

        imprimirDetalles(empleado);
    }
    
    
    public static void imprimirDetalles(Empleado empleado){
        
        String resultado = null;
        
        System.out.println("\nDetalle : " + empleado.obtenerDetalles());
        
        if (empleado instanceof Escritor) {
            
            Escritor escritor = (Escritor) empleado;
            
            resultado = escritor.getTipoDeEscrituraEnTexto();
            
            resultado = ((Escritor) empleado).tipoEscritura.getDescription();
            
            System.out.println("resultado tipoEscritura" + resultado);
        } else if (empleado instanceof Gerente) {
            resultado = ((Gerente ) empleado).getDepartamento();
            System.out.println("resultado departamento " + resultado);
        }
    }
}
